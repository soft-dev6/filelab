/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hanam
 */
public class WriteFriendList {
    public static void main(String[] args) {
            FileOutputStream fos = null;
        try {
            LinkedList<Friend> friendlist = new LinkedList();
            friendlist.add(new Friend("Worawit",43,"085452359"));
            friendlist.add(new Friend("Jakkaman",43,"0824523244"));
            friendlist.add(new Friend("Adsadawut",43,"0925035906"));
            //friend.dat
            File file = new File("List_of_friend.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream  oos= new ObjectOutputStream(fos);
            oos.writeObject(friendlist);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
